import { User } from "@generated/type-graphql";
import argon from "argon2";
import { Args, Ctx, Mutation, Query, Resolver } from "type-graphql";
import {
  LoginInput,
  MyContext,
  RegisterInput,
  UserResponse,
} from "../lib/types";

@Resolver(() => User)
export class UserResolver {
  @Query(() => User, { nullable: true })
  async me(@Ctx() { db, req }: MyContext): Promise<User | null> {
    if (!req.session.userId) {
      return null;
    }

    const user = await db.user.findUnique({
      where: { id: req.session.userId },
    });

    if (!user) {
      return null;
    }

    const { password: _, ...rest } = user;

    return rest;
  }

  @Mutation(() => UserResponse)
  async login(
    @Args() { email, password }: LoginInput,
    @Ctx() { db, req }: MyContext
  ): Promise<UserResponse> {
    const user = await db.user.findUnique({ where: { email } });

    if (!user) {
      return {
        errors: [
          {
            field: "email",
            message: "Invalid email or password",
          },
          {
            field: "password",
            message: "Invalid email or password",
          },
        ],
      };
    }

    const match = await argon.verify(user.password, password);

    if (!match) {
      return {
        errors: [
          {
            field: "email",
            message: "Invalid email or password",
          },
          {
            field: "password",
            message: "Invalid email or password",
          },
        ],
      };
    }

    const { password: _, ...rest } = user;

    req.session.userId = user.id;

    return { user: rest };
  }

  @Mutation(() => UserResponse)
  async register(
    @Args() { email, password }: RegisterInput,
    @Ctx() { db, req }: MyContext
  ): Promise<UserResponse> {
    let user!: User;

    try {
      const hashedPassword = await argon.hash(password);

      user = await db.user.create({
        data: {
          email,
          password: hashedPassword,
        },
      });
    } catch (error) {
      if ((error as any).code === "P2002")
        return {
          errors: [
            {
              field: "email",
              message: "Email already in use",
            },
          ],
        };
    }

    req.session.userId = user.id;

    return { user };
  }
}
