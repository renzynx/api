import { ApolloServer } from "@apollo/server";
import { expressMiddleware } from "@apollo/server/express4";
import { ApolloServerPluginDrainHttpServer } from "@apollo/server/plugin/drainHttpServer";
import { ApolloServerPluginLandingPageDisabled } from "@apollo/server/plugin/disabled";
import { ApolloServerPluginLandingPageGraphQLPlayground } from "@apollo/server-plugin-landing-page-graphql-playground";
import { json } from "body-parser";
import cors from "cors";
import express from "express";
import http from "http";
import { buildSchema } from "type-graphql";
import { Logger } from "./lib/logger";
import "./lib/setup";
import type { MyContext } from "./lib/types";
import { __prod__ } from "./lib/constants";
import { UserResolver } from "./resolvers/user.resolver";
import { PrismaClient } from "@prisma/client";
import session from "express-session";
import Redis from "ioredis";
import connectRedis from "connect-redis";

const bootstrap = async () => {
  const port = process.env.PORT || 3333;
  const app = express();
  const db = new PrismaClient();
  const redis = new Redis(process.env.REDIS_URL!);
  const RedisStore = connectRedis(session);

  const httpServer = http.createServer(app);

  const server = new ApolloServer<MyContext>({
    schema: await buildSchema({ resolvers: [UserResolver], validate: false }),
    plugins: [
      ApolloServerPluginDrainHttpServer({ httpServer }),
      __prod__
        ? ApolloServerPluginLandingPageDisabled()
        : ApolloServerPluginLandingPageGraphQLPlayground({
            settings: { "request.credentials": "include" },
          }),
    ],
  });

  await server.start();

  app.set("trust proxy", 1);
  app.use(
    session({
      name: process.env.SESSION_NAME || "auth",
      secret: process.env.SESSION_SECRET!,
      resave: false,
      saveUninitialized: false,
      cookie: {
        httpOnly: true,
        secure: __prod__,
        sameSite: "lax",
        maxAge: 1000 * 60 * 60 * 24 * 30, // 30 days
        domain: __prod__
          ? process.env.CORS_ORIGIN!.replace(/^https?:\/\//, "")
          : undefined,
      },
      store: new RedisStore({
        client: redis,
        disableTouch: true,
      }),
    })
  );
  app.use(
    "/",
    cors<cors.CorsRequest>(),
    json(),
    expressMiddleware(server, {
      context: async ({ req, res }) => ({ req, res, db }),
    })
  );

  await new Promise<void>((resolve) => httpServer.listen({ port }, resolve));
  Logger.info(`🚀 Server ready at http://localhost:${port}/`, "Server");
};

bootstrap();
