import { join } from "path";

export const __prod__ = process.env.NODE_ENV === "production";

export const rootDir = join(__dirname, "..", "..");
