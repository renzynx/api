import { cyan, redBright, gray, yellowBright } from "colorette";

export const Logger = {
  info: (message: string, ctx?: string) =>
    console.log(
      cyan(`[INFO] ${yellowBright(ctx ? `[${ctx}]` : "")} ${gray(message)}`)
    ),
  error: (message: string, ctx?: string) =>
    console.log(
      redBright(
        `[ERROR] ${yellowBright(ctx ? `[${ctx}]` : "")} ${gray(message)}`
      )
    ),
};
