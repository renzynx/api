import { User } from "@generated/type-graphql";
import type { Prisma, PrismaClient } from "@prisma/client";
import {
  IsEmail,
  IsNotEmpty,
  Matches,
  MaxLength,
  MinLength,
} from "class-validator";
import type { Request, Response } from "express";
import type { Session, SessionData } from "express-session";
import { ArgsType, Field, ObjectType } from "type-graphql";

export interface MyContext {
  req: Request & {
    session: Session & Partial<SessionData> & { userId?: number };
  };
  res: Response;
  db: PrismaClient<
    Prisma.PrismaClientOptions,
    never,
    Prisma.RejectOnNotFound | Prisma.RejectPerOperation | undefined
  >;
}

@ArgsType()
export class LoginInput {
  @Field()
  @IsNotEmpty({ message: "Email Address is required" })
  @IsEmail({}, { message: "Email Address must be valid" })
  @MaxLength(30, { message: "Email Address must be less than 30 characters" })
  email!: string;

  @Field()
  password!: string;
}

@ArgsType()
export class RegisterInput {
  @Field()
  @IsNotEmpty({ message: "Email Address is required" })
  @IsEmail({}, { message: "Email Address must be valid" })
  @MaxLength(30, { message: "Email Address must be less than 30 characters" })
  email!: string;

  @Field()
  @IsNotEmpty({ message: "Password is required" })
  @MinLength(8, { message: "Password be at least 8 characters" })
  @MaxLength(128, { message: "Password be at most 128 characters" })
  @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])/, {
    message:
      "Password must contain at least one uppercase letter, one lowercase letter, one number, and one special character",
  })
  password!: string;
}

@ObjectType()
export class UserResponse {
  @Field(() => User, { nullable: true })
  user?: User;

  @Field(() => [FieldError], { nullable: true })
  errors?: FieldError[];
}

@ObjectType()
export class FieldError {
  @Field()
  field!: string;

  @Field()
  message!: string;
}
