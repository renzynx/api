import "reflect-metadata";
import { config } from "dotenv";
import { join } from "path";
import { rootDir } from "./constants";

config({ path: join(rootDir, ".env") });
